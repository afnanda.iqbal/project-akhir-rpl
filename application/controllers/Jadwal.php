<?php
class Jadwal extends CI_Controller{
	public function index(){

	}
	public function search(){
		$this->session->set_userdata('cart',array());
		$from = $_GET['from'];
		$date_g = $_GET['date_g'];

		$ps = $_GET['ps'];
		$data['date_g'] = $_GET['date_g'];
		if(empty($date_g)){
			redirect('home');
		}
		$data['ps'] = $_GET['ps'];
		$c = $_GET['class'];
		if(!empty($c)){
			$gClass = $this->order_model->Tampil_data('daftar_kelas',array('id_daftar_kelas'=>$c))->row();
			$class = $gClass->class_name;
		}else{
			$class = 'Semua Kelas';
		}
		$data['class'] = $class;
		$data['from'] = $this->order_model->gKategoriW(array('id_kategori'=>$from))->row();


		$data['berangkat'] = $this->order_model->searchJadwal($date_g,$from,$c);

		$data['title'] = 'Pencarian Jadwal';
		$data['content'] = 'jadwal/search';
		$this->load->view('template',$data);
	}
}
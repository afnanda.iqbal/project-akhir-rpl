<?php
class Account extends CI_Controller{
	public function __construct() {
		parent::__construct();
		$user = $this->session->userdata('auth_user');
		if($user){
			redirect('home');
		}
	}
	public function login(){
		$data['title'] = 'Masuk';
		$data['content'] = 'account/login';
		$this->load->view('template',$data);
	}
	public function register(){
		$data['title'] = 'Register';
		$data['content'] = 'account/register';
		$this->load->view('template',$data);
	}

}
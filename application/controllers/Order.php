<?php
	class Order extends CI_Controller{
		public function index(){		
	        $user = $this->session->userdata('auth_user');
	        if(!$user){
	            redirect('home');
	        }
			$data['title'] = 'Konfirmasi Pesanan';
			$data['content'] = 'order';
			$data['cart'] = $this->session->userdata('cart');
			$this->load->view('template',$data);
		}
		public function checkout(){		
	        $user = $this->session->userdata('auth_user');
	        if(!$user){
	            redirect('home');
	        }		
			$data['title'] = 'Konfirmasi Pesanan';
			$data['content'] = 'checkout';
			$data['cart'] = $this->session->userdata('cart');
			$this->load->view('template',$data);
		}
		public function Cart(){
			$id_tempat = $_POST['id_tempat'];
			$jumlah = $_POST['jumlah'];
			$get = $this->order_model->Tampil_data('tempat',array('id_tempat'=>$id_tempat))->row();
			$harga = $get->price*$jumlah;
			if(empty($this->session->userdata('cart'))){
				$this->session->set_userdata('cart',array());
			}
			$old = $this->session->userdata('cart');
			array_push($old, array('id_tempat'=>$id_tempat,'jumlah'=>$jumlah,'harga'=>$harga));
			$this->session->set_userdata('cart',$old);
			// print_r($this->session->userdata('cart'));
		}
		public function dCart(){
			$hackers =  $this->session->userdata('cart');
			$id_tempat = $_POST['id_tempat'];
			foreach($hackers as $key => $value){
				$search = $id_tempat;
				if($value['id_tempat']==$search){
					$old = $this->session->userdata('cart');
					unset($old[$key]);
					$this->session->set_userdata('cart',$old);
				}
			}
		}
		public function cekCart(){
			print_r($this->session->userdata('cart'));
			count($this->session->userdata('cart'));
		}
		public function cekPP(){
			$cek = count($this->session->userdata('cart'));
			if($cek>0){
				$result = 1;
			}else{
				$result = 0;
			}
			echo $result;
		}
		public function check_code($code,$harga){		
	        $user = $this->session->userdata('auth_user');
	        if(!$user){
	            redirect('home');
	        }
			$cek = $this->order_model->Tampil_data('promo_code',array('promo_code'=>$code));
			if($cek->num_rows()==0){
				$result = array('result'=>0);
			}else{
				$d = $cek->row();
	            if($d->promo_price!=0){
	                $total = $harga-$d->promo_price;
	            }elseif($d->promo_percentage!=0){
	                $total = $harga-($harga*$d->promo_percentage/100);
	            }else{
	                $total = $harga;
	            }
	            $min = rupiah($harga-$total);
				$result = array('result'=>1,'total'=>rupiah($total),'totall'=>$total,'code'=>$code,'min'=>$min);
			}
			echo json_encode($result);
		}
		public function tabelconfirm(){
			$cart = $this->session->userdata('cart');
			echo '
          <table class="oconfirm">
              <tbody>';
              foreach($cart as $c){
        	$i = $this->order_model->gTempatW($c['id_tempat']);
                echo'<tr>
                  <td style="text-align:center">
                    <span class="t">'.stime($i[0]->depart_time).'</span>
					<i style="margin-left: 50px; margin-right: 50px" class="material-icons inline-text">arrow_forward</i></a>
				  	<span class="t">'.stime($i[0]->arrive_time).'</span>
				</td>
                  <td>
                    <b class="price-text">'.rupiah($i[0]->price).'</b>
                  </td>				  
                </tr>';
            	}
                echo'
              </tbody>
            </table>
            ';
		}
		public function pay(){		
	        $user = $this->session->userdata('auth_user');
	        if(!$user){
	            redirect('home');
	        }
			$order_time = date('H:i:s');
			$order_date = date('Y-m-d');
			if($this->session->userdata('auth_user')){
				$id_costumer = $this->session->userdata('auth_user');
			}else{
				$id_costumer = 0;
			}
			$payment_type = $_POST['method'];
			if($payment_type==2){
				$status = 'Terbayar';
			}else{
				$status = 'Tertunda';
			}
			$buyer_name = $_POST['buyer_name'];
			$buyer_email = $_POST['buyer_email'];
			$buyer_phone = $_POST['buyer_phone'];
			$promo_code = $_POST['promo_code'];
			$cek = $this->order_model->Tampil_data('promo_code',array('promo_code'=>$promo_code));
			$d = $cek->row();	
			$cart = $this->session->userdata('cart');
			$final_price = 0;
			$id_promo_code = 0;
    		$jml = 0;
			foreach($cart as $c){
      			$jml = $c['jumlah'];
				$q = $this->order_model->Tampil_data('tempat',array('id_tempat'=>$c['id_tempat']))->row();		
				if($cek->num_rows()==0){
					$final_price = $final_price+$q->price;
					$id_promo_code = 0;
				}else{
		            if($d->promo_price!=0){
		                $final_price = $final_price+$q->price-$d->promo_price;
		            }elseif($d->promo_percentage!=0){
		                $final_price = $final_price+$q->price-($q->price*$d->promo_percentage/100);
		            }else{
		                $final_price = $final_price+$q->price;
		            }
		            $id_promo_code = $d->id_promo_code;
				}
			}
			$input = array('order_date'=>$order_date,'order_time'=>$order_time,'order_time'=>$order_time,'id_costumer'=>$id_costumer,'id_promo_code'=>$id_promo_code,'final_price'=>$final_price,'id_payment_type'=>$payment_type,'buyer_name'=>$buyer_name,'buyer_phone'=>$buyer_phone,'buyer_email'=>$buyer_email,'status'=>$status);
			$in = $this->order_model->Tambah_data('order',$input);

		    for ($i=1; $i <= $jml ; $i++) {
		    	
		    	$p_full_name = $_POST['p_full_name'.$i];

		    	$p_title = $_POST['p_title'.$i];

		    	$ticket_code = 'TG'.strtoupper(ranS(9,'all'));

		    	$this->order_model->Tambah_data('passenger',array('id_order'=>$in,'p_full_name'=>$p_full_name,'p_title'=>$p_title,'ticket_code'=>$ticket_code));
		    }

			foreach($cart as $c){
				$q = $this->order_model->Tampil_data('tempat',array('id_tempat'=>$c['id_tempat']))->row();
				$reservation_code = ranS(10,'number');
				$in_res = $this->order_model->Tambah_data('reservation',array('id_tempat'=>$c['id_tempat'],'reservation_code'=>$reservation_code,'id_order'=>$in));
			}

			if($in){
				$result = array('result'=>1,'id_order'=>$in);
			}else{
				$result = array('result'=>0);
			}
			$this->session->set_userdata('cart',array());
			echo json_encode($result);

		}
		public function complete($id_order){		
	        $user = $this->session->userdata('auth_user');
	        if(!$user){
	            redirect('home');
	        }
			$data['o'] = $this->order_model->gOrder($id_order);
			$data['res'] = $this->order_model->Tampil_data('reservation',array('id_order'=>$id_order))->result();
			$data['title'] = 'Pemesanan Sukses';
			$data['content'] = 'checkout_success';
			$this->load->view('template',$data);

		}
	}
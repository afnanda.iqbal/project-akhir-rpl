<?php
class User extends CI_Controller{

	public function index(){		
		$user = $this->session->userdata('auth_user');
		if(!$user){
			redirect('home');
		}
		$data['title'] = 'Konfirmasi Pesanan';
		$data['content'] = 'order';
		$data['cart'] = $this->session->userdata('cart');
		$this->load->view('template',$data);
	}
	public function checkout(){		
		$user = $this->session->userdata('auth_user');
		if(!$user){
			redirect('home');
		}		
		$data['title'] = 'Konfirmasi Pesanan';
		$data['content'] = 'checkout';
		$data['cart'] = $this->session->userdata('cart');
		$this->load->view('template',$data);
	}
	public function __construct() {
        parent::__construct();
        $user = $this->session->userdata('auth_user');
        if(!$user){
        	$this->session->set_flashdata('pesan','<div class="alert red">Anda harus masuk dulu</div>');
            redirect('home');
        }
    }
	public function profile(){
		$data['info'] = $this->user_model->usrInfo($this->session->userdata('auth_user'));
		$data['title'] = 'Pengaturan Akun';
		$data['content'] = 'user/profile';
		$this->load->view('template',$data);
	}
	public function order(){
		$data['info'] = $this->user_model->usrInfo($this->session->userdata('auth_user'));
		$data['list'] = $this->user_model->Tampil_data('order',array('id_costumer'=>$this->session->userdata('auth_user')))->result();
		$data['title'] = 'Pembelian';
		$data['content'] = 'user/order';
		$this->load->view('template',$data);
	}
	public function order_detail($id_order=''){
		$data['o'] = $this->user_model->gOrder($id_order);
		$data['res'] = $this->user_model->Tampil_data('reservation',array('id_order'=>$id_order))->result();
		$data['title'] = 'Detail Pembelian';
		$data['content'] = 'user/order_detail';
		$this->load->view('template',$data);
	}
	public function order_cancel($id_order=''){
		$data['o'] = $this->user_model->gOrder($id_order);
		$data['res'] = $this->user_model->Tampil_data('reservation',array('id_order'=>$id_order))->result();
		$data['title'] = 'Pembatalan Pemesanan';
		$data['content'] = 'user/order_cancel';
		$data['id_order'] = $id_order;
		$this->load->view('template',$data);
	}
	public function p_cancel(){
		$data = $_POST;
		$this->user_model->Tambah_data('order_cancel',$data);
		$this->session->set_flashdata('pesan','<div class="alert orange">Permintaan pembatalan anda telah terkirim, apabila sudah diterima maka pesanan ini akan otomatis dibatalkan</div>');
		redirect('user/order_detail/'.$data['id_order'].'');
	}
	public function p_profile(){
		$data = $_POST;
		$this->user_model->Update('costumer',$data,array('id_costumer'=>$this->session->userdata('auth_user')));
        	$this->session->set_flashdata('pesan','<div class="alert green">Data berhasil disimpan</div>');
		redirect('user/profile');
	}
	public function p_password(){
		$o = md5($_POST['o_password']);
		$p = md5($_POST['password']);
		$n = md5($_POST['c_password']);
		$cek = $this->user_model->usrInfo($this->session->userdata('auth_user'));
		$id_user = $cek->id_user;
		if($cek->password !== $o){
			$this->session->set_flashdata('pesan','<div class="alert red">Kata sandi lama salah</div>');
			redirect('user/profile');
		}elseif($p !== $n){
			$this->session->set_flashdata('pesan','<div class="alert red">Konfirmasi kata sandi tidak sama</div>');
			redirect('user/profile');
		}else{
			$this->user_model->Update('user',array('password'=>$n),array('id_user'=>$id_user));
			$this->session->set_flashdata('pesan','<div class="alert green">Data berhasil disimpan</div>');
			redirect('user/profile');
		}
	}
}
<?php
class Tempat extends CI_Controller{
	public function __construct() {
    	parent::__construct();
        $user = $this->session->userdata('auth_admin');
        if(!$user){
            redirect('admin/login');
        }
    }
    public $table='tempat'; 
    public $page='tempat'; 
    public $primary_key='id_tempat'; 
	public function index($action='',$id=''){
	$data['title'] = 'Detail';
	$data['content'] = 'admin/crud_basic';
	$data['tableTitle'] = array('Tanggal Pijat','Waktu Pijat Mulai','Waktu Pijat Selesai','Jenis Pijat','Tipe Pijat','Masalah Anak','Harga');
	$data['tableField'] = array('depart_at','depart_time','arrive_time','kategori_name_from','rumah_type_name','daftar_name','price');
	$data['inputType'] = array(
								array('type'=>'text','label'=>'Tanggal Pijat','name'=>'depart_at','class'=>'datepicker'),
								array('type'=>'text','label'=>'Waktu Pijat Mulai','name'=>'depart_time','class'=>'timepicker'),
								array('type'=>'text','label'=>'Waktu Pijat Selesai','name'=>'arrive_time','class'=>'timepicker'),
								array('type'=>'select','label'=>'Tipe Pijat','name'=>'id_rumah_type'
									,'option'=>array('data'=>'database','table'=>'rumah_type','label'=>'rumah_type_name','value'=>'id_rumah_type'),'onchange'=>'pTempat()','id'=>'id_rumah_type'),
								array('type'=>'select','label'=>'Jenis Pijat','name'=>'id_kategori_from'
									,'option'=>array('data'=>'ajax','table'=>'kategori','label'=>'kategori_name','value'=>'id_kategori'),'onchange'=>'','id'=>'id_kategori_from'),
							
								array('type'=>'select','label'=>'Masalah Anak','name'=>'id_rumah'
									,'option'=>array('data'=>'ajax','table'=>'daftar_tempat','label'=>'daftar_name','value'=>'id_rumah'),'onchange'=>'','id'=>'id_rumah'),
								array('type'=>'number','label'=>'Harga','name'=>'price'),
							); 
		$data['data'] = $this->admin_model->gTempat();
		$x =0;
		foreach($data['data'] as $d){
			$from = $this->user_model->Tampil_data('kategori',array('id_kategori'=>$d->id_kategori_from))->row()->kategori_name;
			
			$data['data'][$x]->kategori_name_from = $from;

			$x++;
		}
		if($action=='edit'&&$id!==''){
           $no=0;
           $getValue = $this->user_model->Tampil_data($this->table,array($this->primary_key=>$id))->row();
           foreach($data['inputType'] as $z){
			$name = $z['name'];
			 $data['inputType'][$no]['value'] = $getValue->$name;
                $no++;
           }
           $data['aidi'] = $id;
		}
		$data['action'] = $action;
		$data['page'] = $this->page;
		$data['primary_key'] = $this->primary_key;
		$this->load->view('admin/template',$data);
	}
	public function data($action,$id=''){
		if($action=='insert'){
			$required = array('depart_at','depart_time','arrive_time','id_kategori_from','price','id_rumah_type','id_rumah_type');
			foreach($required as $f){
				if(empty($_POST[$f])){
					$this->session->set_flashdata('pesan','<div class="alert red">Masih ada data yang kosong</div>');
					redirect('admin/'.$this->page.'/index/add');
				}
			}
			$data = $_POST;
			$this->user_model->Tambah_data($this->table,$data);
			$msg = 'Data berhasil ditambahkan';
		}elseif($action=='update'){
			$data = $_POST;
			$this->user_model->Update($this->table,$data,array($this->primary_key=>$id));
			$msg = 'Data berhasil diubah';
		}elseif($action=='delete'){
			$this->user_model->Delete($this->table,array($this->primary_key=>$id));
			$msg = 'Data berhasil dihapus';
		}else{
			$msg = '';
		}
		$this->session->set_flashdata('pesan','<div class="alert green">'.$msg.'</div>');
		redirect('admin/'.$this->page.'');
	}
    public function truncate(){
        $this->admin_model->emDB($this->table);
		$this->session->set_flashdata('pesan','<div class="alert green">Data berhasil dikosongkan</div>');
		redirect('admin/'.$this->page.'');
    }
	public function gKategori(){
		$id_type = $_POST['id_type'];
		$get = $this->user_model->Tampil_data('kategori',array('id_rumah_type'=>$id_type))->result();
			echo '<option value="">Pilih Jenis Pijat</option>';
		foreach($get as $g){
			echo '<option value="'.$g->id_kategori.'">'.$g->kategori_name.'</option>';
		}
	}
	public function gTemp(){
		$id_type = $_POST['id_type'];
		$get = $this->user_model->Tampil_data('daftar_tempat',array('id_rumah_type'=>$id_type))->result();
			echo '<option value="">Pilih Masalah Anak</option>';
		foreach($get as $g){
			echo '<option value="'.$g->id_rumah.'">'.$g->daftar_name.'</option>';
		}
	}
}

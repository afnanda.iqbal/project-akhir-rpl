<?php
class Daftar_kelas extends CI_Controller{
	public function __construct() {
    	parent::__construct();
        $user = $this->session->userdata('auth_admin');
        if(!$user){
            redirect('admin/login');
        }
    }
    public $table='daftar_kelas'; 
    public $page='daftar_kelas'; 
    public $primary_key='id_daftar_kelas'; 
	public function index($action='',$id=''){
		$data['title'] = 'Kelas Tempat';
		$data['content'] = 'admin/crud_basic';
		$data['tableTitle'] = array('Nama Kelas','Tipe Pijat');
		$data['tableField'] = array('class_name','rumah_type_name');
		$data['inputType'] = array(
								array('type'=>'text','label'=>'Nama Kelas','name'=>'class_name'),
								array('type'=>'select','label'=>'Tipe Pijat','name'=>'id_rumah_type'
									,'option'=>array('data'=>'database','table'=>'rumah_type','label'=>'rumah_type_name','value'=>'id_rumah_type'),'onchange'=>'','id'=>'id_rumah_type'),
							); 
		$data['data'] = $this->admin_model->gDafta_kelas();
		if($action=='edit'&&$id!==''){
           $no=0;
           $getValue = $this->user_model->Tampil_data($this->table,array($this->primary_key=>$id))->row();
           foreach($data['inputType'] as $z){
			$name = $z['name'];
			 $data['inputType'][$no]['value'] = $getValue->$name;
                $no++;
           }
           $data['aidi'] = $id;
		}
		$data['action'] = $action;
		$data['page'] = $this->page;
		$data['primary_key'] = $this->primary_key;
		$this->load->view('admin/template',$data);
	}
	
	public function data($action,$id=''){
		if($action=='insert'){
			$required = array('class_name','id_rumah_type');
			foreach($required as $f){
				if(empty($_POST[$f])){
					$this->session->set_flashdata('pesan','<div class="alert red">Masih ada data yang kosong</div>');
					redirect('admin/'.$this->page.'/index/add');
				}
			}
			$data = $_POST;
			$this->user_model->Tambah_data($this->table,$data);
			$msg = 'Data berhasil ditambahkan';
		}elseif($action=='update'){
			$data = $_POST;
			$this->user_model->Update($this->table,$data,array($this->primary_key=>$id));
			$msg = 'Data berhasil diubah';
		}elseif($action=='delete'){
			$this->user_model->Delete($this->table,array($this->primary_key=>$id));
			$msg = 'Data berhasil dihapus';
		}else{
			$msg = '';
		}
		$this->session->set_flashdata('pesan','<div class="alert green">'.$msg.'</div>');
		redirect('admin/'.$this->page.'');
	}

    public function truncate(){
        $this->admin_model->emDB($this->table);
		$this->session->set_flashdata('pesan','<div class="alert green">Data berhasil dikosongkan</div>');
		redirect('admin/'.$this->page.'');
    }
}
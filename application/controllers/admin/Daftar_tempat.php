<?php
class Daftar_tempat extends CI_Controller{
	public function __construct() {
    	parent::__construct();
        $user = $this->session->userdata('auth_admin');
        if(!$user){
            redirect('admin/login');
        }
    }
	public function index($action='',$id=''){
	$data['title'] = 'Daftar Tempat';
	$data['content'] = 'admin/crud_basic';
	$data['tableTitle'] = array('Masalah Anak','Kelas Pijat','Tipe Pijat','Jumlah Ruangan Tersedia');
	$data['tableField'] = array('daftar_name','class_name','rumah_type_name','seat_qty');
	$data['inputType'] = array(
									
									array('type'=>'text','label'=>'Masalah Anak','name'=>'daftar_name'),
									array('type'=>'select','label'=>'Tipe Pijat','name'=>'id_rumah_type'
										,'option'=>array('data'=>'database','table'=>'rumah_type','label'=>'rumah_type_name','value'=>'id_rumah_type'),'onchange'=>'pTemp()','id'=>'id_rumah_type'),
									array('type'=>'select','label'=>'Kelas Pijat','name'=>'id_daftar_kelas'
										,'option'=>array('data'=>'ajax','table'=>'daftar_kelas','label'=>'class_name','value'=>'id_daftar_kelas'),'onchange'=>'','id'=>'id_daftar_kelas'),
									array('type'=>'number','label'=>'Jumlah Ruangan Tersedia','name'=>'seat_qty'),
								); 
		$data['data'] = $this->admin_model->gDaftar_tempat();
		if($action=='edit'&&$id!==''){
           $no=0;
           $getValue = $this->user_model->Tampil_data('daftar_tempat',array('id_rumah'=>$id))->row();
           foreach($data['inputType'] as $z){
			   $name = $z['name'];
                $data['inputType'][$no]['value'] = $getValue->$name;
                $no++;
           }
           $data['aidi'] = $id;
		}
		$data['action'] = $action;
		$data['page'] = 'daftar_tempat';
		$data['primary_key'] = 'id_rumah';
		$this->load->view('admin/template',$data);
	}
	public function data($action,$id=''){
		if($action=='insert'){
			$required = array('daftar_name','id_rumah_type','id_daftar_kelas','seat_qty');
			foreach($required as $f){
				if(empty($_POST[$f])){
					$this->session->set_flashdata('pesan','<div class="alert red">Masih ada data yang kosong</div>');
					redirect('admin/daftar_tempat/index/add');
				}
			}
			$data = $_POST;
			$this->user_model->Tambah_data('daftar_tempat',$data);
			$msg = 'Data berhasil ditambahkan';
		}elseif($action=='update'){
			$data = $_POST;
			$this->user_model->Update('daftar_tempat',$data,array('id_rumah'=>$id));
			$msg = 'Data berhasil diubah';
		}elseif($action=='delete'){
			$this->user_model->Delete('daftar_tempat',array('id_rumah'=>$id));
			$msg = 'Data berhasil dihapus';
		}else{
			$msg = '';
		}
		$this->session->set_flashdata('pesan','<div class="alert green">'.$msg.'</div>');
		redirect('admin/daftar_tempat');
	}
	public function gClass(){
		$id_type = $_POST['id_type'];
		$get = $this->user_model->Tampil_data('daftar_kelas',array('id_rumah_type'=>$id_type))->result();
			echo '<option value="">Pilih Kelas Pijat</option>';
		foreach($get as $g){
			echo '<option value="'.$g->id_daftar_kelas.'">'.$g->class_name.'</option>';
		}
	}

    public function truncate(){
        $this->admin_model->emDB('daftar_tempat');
		$this->session->set_flashdata('pesan','<div class="alert green">Data berhasil dikosongkan</div>');
		redirect('admin/daftar_tempat');
    }
}
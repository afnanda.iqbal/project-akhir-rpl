<?php
class Order extends CI_Controller{
	public function __construct() {
    	parent::__construct();
        $user = $this->session->userdata('auth_admin');
        if(!$user){
            redirect('admin/login');
        }
    }
    public $table='order'; 
    public $page='order'; 
    public $primary_key='id_order'; 
	public function index($action='',$id=''){
		$data['title'] = 'Pesanan';
		$data['content'] = 'admin/crud_custom2';
		$data['tableTitle'] = array('Pembeli','Waktu','Harga','Status');
		$data['tableField'] = array('buyer_name','time','price','pay_status');
		$data['data'] = $this->order_model->gOrderA();
		if($action=='edit'&&$id!==''){
           $no=0;
           $getValue = $this->order_model->Tampil_data($this->table,array($this->primary_key=>$id))->row();
           foreach($data['inputType'] as $z){
            $name = $z['name'];
                   $data['inputType'][$no]['value'] = $getValue->$name;
                $no++;
           }
           $data['aidi'] = $id;
		}
		$data['action'] = $action;
		$data['page'] = $this->page;
		$data['primary_key'] = $this->primary_key;
		$this->load->view('admin/template',$data);
	}
	public function data($action,$id=''){
		if($action=='accept'){			
			$data = $this->order_model->Tampil_data('order',array('id_order'=>$id))->row();
			$this->order_model->Update('order',array('status'=>'Terbayar'),array('id_order'=>$id));
			$msg = 'Pesanan #'.$data->id_order.' berhasil di terima';
		
		}elseif($action=='delete'){
			$this->order_model->Delete($this->table,array($this->primary_key=>$id));
			$msg = 'Data berhasil dihapus';
		}else{
			$msg = '';
		}
		$this->session->set_flashdata('pesan','<div class="alert green">'.$msg.'</div>');
		redirect('admin/'.$this->page.'');
	}
  public function truncate(){
    $this->admin_model->emDB($this->table);
    $this->session->set_flashdata('pesan','<div class="alert green">Data berhasil dikosongkan</div>');
    redirect('admin/'.$this->page.'');
}
}
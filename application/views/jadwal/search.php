<main>
  <div class="page-header pink lighten-3 white-text">
    <h4 class="light">Hasil Pencarian Jadwal</h4>
  </div>
  <div class="page-content">
   <div class="container center">
   
      
      <?php if(!isset($berangkat)){ ?>
      <div style="display: <?php if(!empty($_SESSION['cart'])){echo "block";}else{echo "none";} ?>" id="orderBtn">
        <div class="fixed-action-btn">
          <a id="btnPesan" href="#confirm" class="btn btn-large pink lighten-3 animated modal-trigger">
            <i class="large material-icons inline-text">shopping_cart</i> Pesan Tiket
          </a>
        </div>
      </div>
      <div class="col s12 m12">
        <div class="card pink lighten-4">
          <div class="card-content">
            <span class="card-title">
       
            </span>
            <div class="row">
              <div class="col s3">
                <span class="light"><i class="material-icons inline-text">date_range</i> <?=hari_tgl($date_g)?></span>
              </div>
              <div class="col s2">
                <span class="light"><i class="material-icons inline-text">groups</i> <?=$ps?> Anak</span>
              </div>
              <div class="col s2">
               <span class="light"><i class="material-icons inline-text">airline_seat_recline_normal</i> <?=$class?></span>
             </div>
           </div>

           <table class="search">
            <thead class="pink lighten-3 white-text">
              <tr>
                  <th>Mulai</th>
                  <th></th>
                  <th>Durasi Pijat</th>
                  <th width="15%">Harga Per Orang</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              if(empty($berangkat)){
                ?>
                <tr>
                  <td colspan="6">
                    <center>
                      <i style="font-size: 50px" class="material-icons">hourglass_empty</i><br>
                      <h5 class="light">Pencarian tidak ada hasil</h5>
                    </center>
                  </td>
                </tr>
                <?php
              }else{
                foreach($berangkat as $b){?>
                <tr>
                  <td style="text-align:center">
                    <span class="t"><?=stime($b->depart_time)?></span>
                    <p><?=$b->kategori_name_from?></p>
                  </td>
                  <td style="text-align:center">
                    <i class="material-icons">keyboard_arrow_right</i>
                  </td>
        
                  <td>
                    <span class="t"><?=sel_jam($b->depart_at.' '.$b->depart_time,$b->arrive_time)?></span>
                    <!-- <p>Langsung</p> -->
                  </td>
                  <td style="text-align:center">
                    <span class="price-text-5"><?=rupiah($b->price)?></span>
                    <?php if(!empty($_SESSION['cart'])){
                      foreach($_SESSION['cart'] as $c){
                        if($c['id_tempat']==$b->id_tempat){
                          $text='BATAL';
                          $onclick='delCart';
                          $disabled = '';
                        }else{
                          $text='PILIH';
                          $onclick='addCart';
                          $disabled=' disabled';
                        }
                      }
                      ?>
                      <a id="b<?=$b->id_tempat?>" onclick="<?=$onclick?>(<?=$b->id_tempat?>,<?=$ps?>)" style="width: 100%" class="btn pink lighten-3 pilih<?=$disabled?>"><?=$text?></a>
                      <?php }else{
                        ?>
                        <a id="b<?=$b->id_tempat?>" onclick="addCart(<?=$b->id_tempat?>,<?=$ps?>)" style="width: 100%" class="btn pink lighten-3 pilih">Pilih</a>
                        <?php
                      } ?>
                    </td>
                  </tr>
                  <?php }
                } ?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
      <?php }else{
        ?>
        <div style="display: none" id="orderBtn">
          <div class="fixed-action-btn">
            <a id="btnPesan" href="#confirm" class="btn btn-large pink lighten-3 animated modal-trigger">
              <i class="large material-icons inline-text">shopping_cart</i> Pesan Jadwal
            </a>
          </div>
        </div>
        <div class="col s12 m12">
          <div class="card grey lighten-4">
            <div class="card-content">
              <span class="card-title">
              
              </span>
              <div class="row">
                <div class="col s6 m2">
                  <span class="light"><i class="material-icons inline-text">child_care</i> <?=$ps?> Anak</span>
                </div>
                <div class="col s6 m2">
                 <span class="light"><i class="material-icons inline-text">access_time</i> <?=$class?></span>
               </div>
             </div>
           </div>
         </div>
       </div>
       <div class="col s12 m6">
        <div class="card grey lighten-4">
          <div class="card-content">
            <span class="card-title">Jadwal Pijat</span>
            <div class="row">
              <div class="col m12 s12">
                <span class="light"><i class="material-icons inline-text">date_range</i> <?=hari_tgl($date_g)?></span>
              </div>
            </div>
            
            <table class="search">
              <thead style="text-align: center" class="pink lighten-3 white-text">
                <tr>
                  <th>Pelaksanaan Pijat</th>
                  <th></th>
                  <th>Durasi Pijat</th>
                  <th width="15%">Harga Per Orang</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                if(empty($berangkat)){
                  ?>
                  <tr>
                    <td colspan="6">
                      <center>
                        <i style="font-size: 50px" class="material-icons">hourglass_empty</i><br>
                        <h5 class="light">Pencarian tidak ada hasil</h5>
                      </center>
                    </td>
                  </tr>
                  <?php
                }else{
                  foreach($berangkat as $b){?>
                  <tr>
                    <td style="text-align:center">
                      <span class="t"><?=stime($b->depart_time)?></span>
                      <i style="margin-left: 50px; margin-right: 50px" class="material-icons inline-text">arrow_forward</i></a>
                      <span class="t"><?=stime($b->arrive_time)?></span>
                    </td>
                    <td style="text-align:center">
                      <i class="material-icons">keyboard_arrow_right</i>
                    </td>

                    <td>
                      <span class="t"><?=sel_jam($b->depart_at.' '.$b->depart_time,$b->arrive_time)?></span>
                      <!-- <p>Langsung</p> -->
                    </td>
                    <td style="text-align:center">
                      <span class="price-text-5"><?=rupiah($b->price)?></span>
                      <a id="b<?=$b->id_tempat?>" onclick="addCartB(<?=$b->id_tempat?>,<?=$ps?>)" style="width: 100%" class="btn pink lighten-3 pilih_b">Pilih</a>
                    </td>
                  </tr>
                  <?php }
                } ?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
      <?php
    } ?>
  </div>
</div>
</div>

<div id="confirm" class="modal modal-show">
  <div class="modal-content grey lighten-4">
    <h5 class="light">Detail Pesanan</h5>
    <div id="tblorder"></div>
  </div>
  <div class="modal-footer">
    <a class="white-3s-effect white btn white black-text modal-action modal-close">BATAL</a>
    <a href="<?=site_url('order')?>" class="pink lighten-3s-effect pink lighten-3 btn pink lighten-3 modal-action modal-close">LANJUTKAN</a>
  </div>
</div>
</main>

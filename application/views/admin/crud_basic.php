<main>
  <div class="container">
    <div class="admin-title">
      <div class="row">
        <div class="col m6 s12">
          <h4 class="light"><?=$title?></h4>
        </div>
        <div class="col m6 s12">
         <div class="nav-breadcrumb pink lighten-3">
          <a href="#!" class="breadcrumb">Admin</a>
          <a href="#!" class="breadcrumb"><?=$title?></a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col m7 s12">
      <div class="card grey lighten-4">
        <div class="title-card grey lighten-4"><?=$title?></div>
        <div class="card-content white">
          <div class="container">
            <div class="row">
              <?php
              if($this->session->flashdata('pesan')){
                echo $this->session->flashdata('pesan');
              }else{
                if($action==''){
                  echo '<div class="alert pink lighten-3">Kelola Data '.$title.'</div>';
                }elseif(isset($info)){
                  echo '<div class="alert pink lighten-3 lighten-1">'.$info.'</div>';
                }
              }
              ?>   
            </div>
            <?php if($action!==''){ ?>

            <?php
            if($action=='add'){
              echo form_open('admin/'.$page.'/data/insert');
            }elseif($action=='edit'){
              echo form_open('admin/'.$page.'/data/update/'.$aidi.'');
            }
            ?>  
            <div id="message"></div>
            <?php foreach($inputType as $i){
              ?>
              <div class="row">
                <div class="input-field">
                  <?php if($i['type']=='text'||$i['type']=='number'||$i['type']=='time'||$i['type']=='file'){ ?>
                  <input name="<?=$i['name']?>" <?php if(isset($i['id'])) echo 'id="'.$i['id'].'"'?> type="<?=$i['type']?>" <?php if(isset($i['value'])) echo 'value="'.$i['value'].'"'?> <?php if(isset($i['class'])) echo 'class="'.$i['class'].'"'?>>
                  <?php }elseif($i['type']=='select'){
                    ?>
                    <select id="<?php if(isset($i['id'])) echo $i['id']?>" name="<?=$i['name']?>" <?php if(isset($i['onchange'])) echo 'onchange="'.$i['onchange'].'"'?> <?php if(isset($i['class'])) echo 'class="'.$i['class'].'"'?>>
                      <option value="">Pilih <?=$i['label']?></option>
                      <?php 
                      $o = $i['option'];
                      $value = $o['value'];
                      $label = $o['label'];
                      if($o['data']=='database'){
                        $get = $this->user_model->gPromoCode($o['table'])->result();
                        foreach($get as $gg){
                          if(isset($i['value'])){
                            if($gg->$value==$i['value']){
                              $selected = " selected";
                            }else{
                              $selected = "";
                            }
                          }else{
                            $selected = "";
                          }
                          echo '<option value="'.$gg->$value.'"'.$selected.'>'.$gg->$label.'</option>';
                        }
                      }elseif($o['data']=='custom'){
                        foreach($o['list'] as $gg){
                          echo '<option value="'.$gg['value'].'">'.$gg['label'].'</option>';
                        }
                      }elseif($o['data']=='ajax'){
                        if(isset($i['value'])){
                          $get = $this->user_model->Tampil_data($o['table'],array($i['option']['value']=>$i['value']))->result();
                          foreach($get as $gg){
                            if(isset($i['value'])){
                              if($gg->$value==$i['value']){
                                $selected = " selected";
                              }else{
                                $selected = "";
                              }
                            }else{
                              $selected = "";
                            }
                            echo '<option value="'.$gg->$value.'"'.$selected.'>'.$gg->$label.'</option>';
                          }
                        }
                      }
                      ?>
                    </select>
                    <?php
                  }elseif($i['type']=='textarea'){
                    ?>
                    <textarea class="materialize-textarea <?php if(isset($i['class'])) echo $i['class']?>" name="<?=$i['name']?>"></textarea>
                    <?php
                  } ?>
                  <label><?=$i['label']?></label>
                </div>
              </div>
              <?php } ?>
              <button type="submit" class="btn waves-effect pink lighten-3 pink lighten-3"><i class="material-icons inline-text">save</i> Simpan</button>
              <button type="reset" class="btn waves-effect pink lighten-3 red lighten-1"><i class="material-icons inline-text">replay</i> Reset</button>
            </form>
            <?php }else{
              ?>
              <?php
            } ?>
          </div>
        </div>
      </div>
    </div>
    <div class="col m5 s12">
      <div class="card grey lighten-4">
        <div class="title-card grey lighten-4">Menu <?=$title?></div>
        <div class="card-content white">
          <div class="container">
            <div class="btn-group">
              <a href="<?=site_url('admin/'.$page.'/index/add')?>" class="btn waves-effect pink lighten-3 block"><i class="material-icons inline-text">add_box</i> Tambah Data</a>
              <a href="#truncate" class="btn waves-effect modal-trigger pink lighten-3 block"><i class="material-icons inline-text">delete_forever</i> Hapus Semua Data</a>
              <div id="truncate" class="modal deletemodal">
                <div class="modal-content pink lighten-3 white-text">
                  <p>Apakah anda yakin ingin mengosongkan semua data?</p>
                </div>
                <div class="modal-footer">
                  <a class="waves-effect waves-red btn white pink lighten-3 modal-action modal-close">TIDAK</a>
                  <a href="<?=site_url('admin/'.$page.'/truncate')?>" class="waves-effect waves-green btn pink lighten-3 modal-action modal-close">YA</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col m12 s12">
      <div class="card grey lighten-4">
        <div class="title-card grey lighten-4">Data <?=$title?></div>
        <div class="card-content white">
          <table class="datatables responsive-table striped bordered">
            <thead class="pink lighten-3">
              <tr class="white-text">
                <th class="light">No</th>
                <?php 
                foreach($tableTitle as $tt){
                  echo '<th class="light">'.$tt.'</th>';
                }
                ?>
                <th width="15%" class="light">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach($data as $d){?>
              <tr>
                <td><?=$no?></td>
                <?php 
                foreach($tableField as $tf){
                  echo '<td>'.$d->$tf.'</td>';
                }
                ?>
                <td style="text-align: center">
                  <a href="<?=site_url('admin/'.$page.'/index/edit/'.$d->$primary_key.'')?>" class="btn waves-effect pink lighten-3 action yellow"><i class="material-icons">edit</i></a>
                  <a href="#deleteDialog<?=$d->$primary_key?>" class="btn waves-effect modal-trigger red lighten-1 action modal-trigger red"><i class="material-icons">delete</i></a>
                </td>
                <div id="deleteDialog<?=$d->$primary_key?>" class="modal deletemodal">
                  <div class="modal-content pink lighten-3 white-text">
                    <p>Apakah anda yakin ingin menghapus data ini?</p>
                  </div>
                  <div class="modal-footer">
                    <a class="waves-effect waves-pink lighten-3 btn white pink lighten-3 modal-action modal-close">TIDAK</a>
                    <a href="<?=site_url('admin/'.$page.'/data/delete/'.$d->$primary_key.'')?>" class="waves-effect pink lighten-3 btn red lighten-1 modal-action modal-close">YA</a>
                  </div>
                </div>
                <?php $no++; } ?>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
<!--                     <div class="row">
                      <div class="alert pink lighten-3">Tes </div>
                      <div class="alert green">Tes </div>
                      <div class="alert red">Tes </div>
                      <div class="alert orange">Tes </div>
                      <div class="alert pink lighten-3 strip-pink lighten-3">Tes</div>
                      <div class="alert pink lighten-3 strip-green">Tes</div>
                      <div class="alert pink lighten-3 strip-red">Tes</div>
                      <div class="alert pink lighten-3 strip-orange">Tes</div>
                    </div> -->
                  </div>
                </main>  
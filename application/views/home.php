<main>
 <div class="carousel carousel-slider" data-indicators="true">
  <div class="carousel-fixed-itemr">
  </div>
  <div style="text-align: left" class="carousel-item pink lighten-3 white-text center" href="#one!">
    <div class="container">
    <?php 
    if($this->session->userdata('auth_user')){
      $info = $this->user_model->Tampil_data('costumer',array('id_costumer'=>$this->session->userdata('auth_user')))->row();
      ?>
      <h3 style="margin-top: 100px" class="light">Hai, <?=$info->full_name?></h3>
      <p class="white-text">Sehatkan Bayi Anda Bersama Putri Baby Spa</p>
      <?php
    }else{
      ?>
      <h3 style="margin-top: 100px" class="light">SELAMAT DATANG DI PUTRI BABY SPA</h3>
      <p class="white-text">Temukan Jadwal Pijatmu Di Sini</p>
      <a href="<?=site_url('account/register')?>" class="btn pink lighten-3s-effect pink lighten-2 white-text-2">DAFTAR SEKARANG</a>
      <?php
    }
    ?>
  </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div style="margin-top: -100px" class="col s12 m12 l12">
      <div class="card pink lighten-2">
        <div class="card-tabs">
          <ul class="tabs tabs-fixed-width tabs-transparent tabs-icon">
            <li class="tab"><a href="#test1"><i class="material-icons">event_note</i>Jadwal</a></li>
          </ul>
          
        </div>
        <div class="card-content grey lighten-5">
          <div id="test1"> 
         
          <!-- <img style="width: 200px; float: right; margin-right: 80px;" src="https://i.pinimg.com/564x/1c/1e/02/1c1e027fb36a489289e7053e4a7ffc6e.jpg">  -->
            <form method="GET" action="<?=site_url('jadwal/search')?>">    
              
                <div class="input-field col l4 m6 s12">
                  <i class="material-icons prefix">dialpad</i>
                  <select name="from" id="p_asal" onchange="cekTP()">
                    <option value="">Pilih Kategori</option>
                    <?php
                    foreach($plp as $l){
                      echo '<option value="'.$l->id_kategori.'"> '.$l->kategori_name.'</option>';
                    }
                    ?>
                  </select>
                  <label for="icon_prefix">Kategori</label>
                </div>

                <div class="input-field col l2 m6 s6">
                  <i class="material-icons prefix">access_time</i>       
                  <select name="class">
                    <option value="">Semua</option>
                    <?php
                    foreach($kp as $l){
                      echo '<option value="'.$l->id_daftar_kelas.'">'.$l->class_name.'</option>';
                    }
                    ?>
                  </select>
                  <label>Kelas</label>
                </div>
                <div class="input-field col l2 m6 s6">
                  <i class="material-icons prefix">child_care</i>   
                  <select name="ps">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select>
                  <label>Jumlah Anak</label>
                </div>
              

              <div class="row">
                <div class="input-field col l4 m4 s12">
                  <i class="material-icons prefix">date_range</i>
                  <input type="text" id="p_b" name="date_g" class="datepicker" onchange="cekTGLP()">
                  <label for="icon_prefix">Tanggal Pijat Mulai</label>
                </div>

                <div style="float: right" class="col l4 m4 s12">
                  <button type="submit" style="margin-top:20px;width: 100% " class="pink lighten-3s-effect pink lighten-3 pink lighten-3 btn"><i class="material-icons left">search</i>CARI JADWAL</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>  

</main>  



<!-- <footer class="page-footer pink lighten-3">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Putri Baby Spa</h5>
                <p class="grey-text text-lighten-4">Mriyan Timbulharjo Sewon Bantul</p>
              </div>
              <!-- <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
              </div> -->
            <!-- </div>
          </div>
          <div class="footer-copyright pink lighten-2">
      <div class="container">
        <span>Copyright &copy; 2020 <a class="grey-text text-lighten-4" href="<?= base_url() ?>" target="_blank">Putri Baby Spa</a> All rights reserved.</span>
        <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="<?= base_url() ?>">Apnansap</a></span>
      </div>
          </div>
        </footer> -->
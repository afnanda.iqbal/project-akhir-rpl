<main>
  <div class="page-header pink lighten-3 white-text">
    <h4 class="light">Selesai</h4>
  </div>
  <div class="page-content">
   <div class="container">
    <div class="row">
      <div class="col s12 no-padding">
        <div class="card grey lighten-4">
          <div class="card-tabs">
            <ul class="tabs tabs-fixed-width tabs-transparent">
              <li class="tab disabled"><a class="pink lighten-3" href="#test1"><span class="label pink lighten-3">1</span> Isi Data</a></li>
              <li class="tab disabled"><a class="pink lighten-3" href="#test2"><span class="label pink lighten-3">2</span> Pembayaran</a></li>
              <li class="tab"><a class="pink lighten-3 active" href="#test2"><span class="label pink lighten-3">3</span> Selesai</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col m12 no-padding">
       <div class="card">
        <div class="card-content white">
          <center>
            <h4 class="light"><i style="font-size: 100px" class="material-icons inline-text green-text">check_circle</i>
              <br>Terimakasih, pesanan anda telah kami terima</h4></center>
              <div style="padding-top: 40px" class="row">
                <h5 class="light">Detail Pesanan</h5>
                <table>
                  <thead>
                    <tr>
                      <th>No Pesanan</th>
                      <th>Nama Pemesan</th>
                      <th>Email</th>
                      <th>No.HP</th>
                      <th>Metode Pembayaran</th>
                      <th>Total</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>#<?=$o->id_order?></td>
                      <td><?=$o->buyer_name?></td>
                      <td><?=$o->buyer_email?></td>
                      <td><?=$o->buyer_phone?></td>
                      <td><?=$o->payment_type_name?></td>
                      <td><?=rupiah($o->final_price)?></td>
                      <td><?=$o->status?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
          <div class="row"><!-- 
            <a class="btn pink lighten-3">Button</a>
            <a class="btn pink lighten-3">Button</a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</main>

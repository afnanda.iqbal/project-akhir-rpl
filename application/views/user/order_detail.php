<main>
	<div class="page-content">
   <div class="container">
    <div class="row">
      <div class="col m4">
        <?php $this->load->view('user/nav') ?>
      </div>
      <div class="col m8">
        <div class="card grey lighten-3">
          <?php 
          if($o->status=='Terbayar'){
            $color = "green";
          }elseif($o->status=='Tertunda'){
            $color = "orange";
          }elseif($o->status=='Batal'){
            $color = "red";
          }
          ?>
          <div class="title-card grey lighten-3">Detail Pembelian <span class="right label-flat <?=$color?>"><?=$o->status?></span></div>
          <div class="card-content white">  
            <div class="row">
              <?=$this->session->flashdata('pesan')?>
            </div>
            <div class="row">    
              <?php 

              $tgl = '';

              ?>

              <?php if($tgl<=date('Y-m-d H:i:s'&&$o->status=='Terbayar')){ ?>  
                <a href="<?=site_url('user/order_cancel/'.$o->id_order.'')?>" class="btn red"><i class="material-icons inline-text">not_interested</i> PEMBATALAN</a>
                <?php }else{
                  ?>    
                  <a class="btn red disabled"><i class="material-icons inline-text">not_interested</i> PEMBATALAN</a>
                  <?php
                } ?>
              </div>
              <div>
                <h5 class="light">Detail Pesanan</h5>
                <table>
                  <thead>
                    <tr>
                      <th>No Pesanan</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>No.HP</th>
                      <th>Metode Pembayaran</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>#<?=$o->id_order?></td>
                      <td><?=$o->buyer_name?></td>
                      <td><?=$o->buyer_email?></td>
                      <td><?=$o->buyer_phone?></td>
                      <td><?=$o->payment_type_name?></td>
                      <td><?=rupiah($o->final_price)?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
 
            </div>
          </div>
        </div>
      </div>
    </main>

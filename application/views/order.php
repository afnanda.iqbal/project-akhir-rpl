<main>
  <div class="page-header pink lighten-3 white-text">
    <h4 class="light">Konfirmasi Pesanan</h4>
  </div>
  <div class="page-content">
   <div class="container">
    <div class="row">
      <div class="col s12 m8">
        <?php foreach($cart as $c){
          $i = $this->order_model->gTempatW($c['id_tempat']);
          ?>
          <div class="card grey lighten-4">
            <div class="card-content ">
              <i class="material-icons inline-text green-text">group</i> <?=$c['jumlah']?> Anak | <i class="material-icons inline-text orange-text">access_time</i> <?=$i[0]->class_name?>
            </div>
          </div>

          <div class="card grey lighten-4">
            <div class="title-card grey lighten-4">
              <i class="material-icons inline-text">event_note</i> Detail Jadwal</div>
              <div class="card-content ">
                <b><?=hari_tgl($i[0]->depart_at)?></b><br>
                <table>
                  <tr>
                    </tr>
                  </table>
                  <table class="detail">
                    <tbody>
                      <tr>
                      <td style="text-align:center">
                              <span class="t"><?=stime($i[0]->depart_time)?></span>
                              <i style="margin-left: 50px; margin-right: 50px" class="material-icons inline-text">arrow_forward</i></a>
                              <span class="t"><?=stime($i[0]->arrive_time)?></span>
                              <p style="text-align:center">
                            </td>
                        <td>
                          <span class="t"><?=sel_jam($i[0]->depart_at.' '.$i[0]->depart_time,$i[0]->arrive_time)?></span>
                          <!-- <p>Langsung</p> -->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php } ?>
            </div>
            <div class="col m4">
              <div class="card grey lighten-4">
                <div class="title-card grey lighten-4"><i class="material-icons inline-text">attach_money</i> Rincian Harga</div>
                <div class="card-content ">
                  <table class="light">
                    <?php 
                    $total = 0 ;
                    foreach($cart as $c){
                      $i = $this->order_model->gTempatW($c['id_tempat']);
                      $harga = $i[0]->price*$c['jumlah'];
                      $total = $total+$harga;
                      ?>
                      <tr>
                        <td  style="width:60%"> (Anak) x<?=$c['jumlah']?></td>
                        <td style="text-align: right;"><b><?=rupiah($harga)?></b></td>
                      </tr>
                      <?php } ?>
                      <tr>
                        <td>Harga yang harus anda bayar</td>
                        <td style="text-align: right;"><b><?=rupiah($total)?></b></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <a href="<?=site_url('order/checkout')?>" class="btn pink lighten-3 btn-large" style="width: 100%">
                  <i class="material-icons inline-text">chevron_right</i> Lanjutkan ke Pemesanan</a>
                </div>
              </div>
              <div class="row">
                <div class="col m8">
                </div>
              </div>
            </div>
          </div>
        </main>
<?php
class Admin_model extends CI_Model{

    public function Tampil_data($table,$where){
		return $this->db->select()->from($table)->where($where)->get();
	}
    public function Tambah_data($table,$value){
		$this->db->insert($table,$value);
		return $this->db->insert_id();
	}
    public function Update($table,$value,$where){
		return $this->db->update($table,$value,$where);
	}
	public function Delete($table,$where){
		return $this->db->delete($table,$where);
	}

    public function auth_login_admin($username,$password){
		$get = $this->db->select()->from('user')->where(array('username'=>$username,'password'=>$password,'level_id'=>1))->get()->num_rows();
		if($get==0){
			return false;
		}else{
			return true;
		}
	}
    public function gDaftar_tempat(){
        $this->db->join('daftar_kelas','daftar_kelas.id_daftar_kelas=daftar_tempat.id_daftar_kelas');
        $this->db->join('rumah_type','rumah_type.id_rumah_type=daftar_tempat.id_rumah_type');
        $query = $this->db->get('daftar_tempat');
        return $query->result();
    }
    public function gDafta_kelas(){
        $this->db->join('rumah_type','rumah_type.id_rumah_type=daftar_kelas.id_rumah_type');
        $query = $this->db->get('daftar_kelas');
        return $query->result();
    }
    public function gKategori(){
        $this->db->join('rumah_type','rumah_type.id_rumah_type = kategori.id_rumah_type');
        $query = $this->db->get('kategori');
        return $query->result();
    }
    public function gTempat(){
        $this->db->join('daftar_tempat','daftar_tempat.id_rumah = tempat.id_rumah');
        $this->db->join('rumah_type','rumah_type.id_rumah_type = tempat.id_rumah_type');
        $query = $this->db->get('tempat');
        return $query->result();
    }
    public function emDB($table){
        return $this->db->truncate($table);
        }
 }
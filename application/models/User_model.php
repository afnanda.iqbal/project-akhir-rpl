<?php
 class User_model extends CI_Model{
	 
	public function Tambah_data($table,$value){
		$this->db->insert($table,$value);
		return $this->db->insert_id();
	}
	public function Tampil_data($table,$where){
		return $this->db->select()->from($table)->where($where)->get();
	}

	public function gPromoCode($table){
		return $this->db->get($table);
	}

	public function Update($table,$value,$where){
		return $this->db->update($table,$value,$where);
	}
	public function Delete($table,$where){
		return $this->db->delete($table,$where);
	}

 	public function auth_login($username,$password){
 		$get = $this->db->select()->from('user')->where(array('username'=>$username,'password'=>$password,'level_id'=>2))->get()->num_rows();
 		if($get==0){
 			return false;
 		}else{
 			return true;
 		}
 	}
	
 	public function gIDCostumer($email){
 		return $this->db->get_where('costumer',array('email'=>$email))->row()->id_costumer;
 	}
 	public function gIDUser($email){
 		return $this->db->get_where('costumer',array('email'=>$email))->row()->id_user;
 	}
 	public function usrInfo($id_costumer){
 		$this->db->where('costumer.id_costumer',$id_costumer);
 		$this->db->join('user','user.id_user=costumer.id_user');
 		$query = $this->db->get('costumer');
 		return $query->row();
 	}
	 public function gOrder($id_order){
		$this->db->where('id_order',$id_order);
		$this->db->join('payment_type','payment_type.id_payment_type=order.id_payment_type');
		$query = $this->db->get('order');
		return $query->row();
	}
	public function gOrderW($where){
		$this->db->where($where);
		$this->db->join('payment_type','payment_type.id_payment_type=order.id_payment_type');
		$query = $this->db->get('order')->result();
		$x=0;
		foreach($query as $d){
			if($d->status=='Terbayar'){
				$color = "green";
			}elseif($d->status=='Tertunda'){
				$color = "orange";
			}elseif($d->status=='Batal'){
				$color = "red";
			}
			$query[$x]->time = tgl_indo($d->order_date).' '.stime($d->order_time);
			$query[$x]->price = rupiah($d->final_price);
			$query[$x]->pay_status = '<span class="label-flat '.$color.'">'.$d->status.'</span>';
			$x++;
		}
		return $query;
	}
	public function gOrderTime($start,$end){
		$this->db->where('order_date >=',$start);
		$this->db->where('order_date <=',$end);
		$this->db->join('payment_type','payment_type.id_payment_type=order.id_payment_type');
		$query = $this->db->get('order')->result();
		$x=0;
		foreach($query as $d){
			if($d->status=='Terbayar'){
				$color = "green";
			}elseif($d->status=='Tertunda'){
				$color = "orange";
			}elseif($d->status=='Batal'){
				$color = "red";
			}
			$query[$x]->time = tgl_indo($d->order_date).' '.stime($d->order_time);
			$query[$x]->price = rupiah($d->final_price);
			$query[$x]->pay_status = '<span class="label-flat '.$color.'">'.$d->status.'</span>';
			$x++;
		}
		return $query;
	}
	public function gOrderC(){
		$this->db->join('order','order.id_order=order_cancel.id_order');
		$this->db->join('payment_type','payment_type.id_payment_type=order.id_payment_type');
		$query = $this->db->get('order_cancel')->result();
		$x=0;
		foreach($query as $d){
			if($d->status=='Terbayar'){
				$color = "green";
			}elseif($d->status=='Tertunda'){
				$color = "orange";
			}elseif($d->status=='Batal'){
				$color = "red";
			}
			$query[$x]->time = tgl_indo($d->order_date).' '.stime($d->order_time);
			$query[$x]->price = rupiah($d->final_price);
			$query[$x]->pay_status = '<span class="label-flat '.$color.'">'.$d->status.'</span>';
			$x++;
		}
		return $query;
	}
 	public function gCostumer(){
 		$this->db->join('user','user.id_user=costumer.id_user');
 		$query = $this->db->get('costumer');
 		return $query->result();
 	}
 	public function gKategoriW($where){
 		$this->db->where($where);
 		$this->db->join('rumah_type','rumah_type.id_rumah_type = kategori.id_rumah_type');
 		$query = $this->db->get('kategori');
 		return $query;
 	}
	public function gReservationW($where){
		$this->db->where($where);
		$this->db->join('tempat','tempat.id_tempat=reservation.id_tempat');
		$query = $this->db->get('reservation');
		return $query;
	}
 
}
 
 
 
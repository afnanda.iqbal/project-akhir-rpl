<?php
class Order_model extends CI_Model{

	public function Tampil_data($table,$where){
		return $this->db->select()->from($table)->where($where)->get();
	}
	public function Tambah_data($table,$value){
		$this->db->insert($table,$value);
		return $this->db->insert_id();
	}
	public function gIDCostumer($email){
		return $this->db->get_where('costumer',array('email'=>$email))->row()->id_costumer;
	}
	public function gIDUser($email){
		return $this->db->get_where('costumer',array('email'=>$email))->row()->id_user;
	}
	public function usrInfo($id_costumer){
		$this->db->where('costumer.id_costumer',$id_costumer);
		$this->db->join('user','user.id_user=costumer.id_user');
		$query = $this->db->get('costumer');
		return $query->row();
	}
	public function gCostumer(){
		$this->db->join('user','user.id_user=costumer.id_user');
		$query = $this->db->get('costumer');
		return $query->result();
	}
	public function gKategoriW($where){
		$this->db->where($where);
		$this->db->join('rumah_type','rumah_type.id_rumah_type = kategori.id_rumah_type');
		$query = $this->db->get('kategori');
		return $query;
	}
	public function searchJadwal($date_g,$from,$class){
		$this->db->where('tempat.id_rumah_type',1);
		if(!empty($class)){
			$this->db->where('daftar_tempat.id_daftar_kelas',$class);	
		}
		$this->db->where('depart_at',$date_g);
		$this->db->where('id_kategori_from',$from);
		$this->db->join('daftar_tempat','daftar_tempat.id_rumah=tempat.id_rumah');
		$this->db->join('daftar_kelas','daftar_kelas.id_daftar_kelas=daftar_tempat.id_daftar_kelas');
		$query = $this->db->get('tempat')->result();
		$x=0;
		foreach($query as $d){
			$p_from = $this->gKategoriW(array('id_kategori'=>$from))->row();
			$x++;
		}
		return $query;
	}

	public function gTempatW($id_tempat){
		$this->db->where('id_tempat',$id_tempat);
		$this->db->join('daftar_tempat','daftar_tempat.id_rumah=tempat.id_rumah');
		$this->db->join('daftar_kelas','daftar_kelas.id_daftar_kelas=daftar_tempat.id_daftar_kelas');
		$query = $this->db->get('tempat')->result();
		$x=0;
		foreach($query as $d){
			$p_from = $this->gKategoriW(array('id_kategori'=>$d->id_kategori_from))->row();
			$p_from->kategori_name;
			$x++;
		}
		return $query;
	}

	public function gOrder($id_order){
		$this->db->where('id_order',$id_order);
		$this->db->join('payment_type','payment_type.id_payment_type=order.id_payment_type');
		$query = $this->db->get('order');
		return $query->row();
	}
	public function gOrderA(){
		$this->db->join('payment_type','payment_type.id_payment_type=order.id_payment_type');
		$query = $this->db->get('order')->result();
		$x=0;
		foreach($query as $d){
			if($d->status=='Terbayar'){
				$color = "green";
			}elseif($d->status=='Tertunda'){
				$color = "orange";
			}elseif($d->status=='Batal'){
				$color = "red";
			}
			$query[$x]->time = tgl_indo($d->order_date).' '.stime($d->order_time);
			$query[$x]->price = rupiah($d->final_price);
			$query[$x]->pay_status = '<span class="label-flat '.$color.'">'.$d->status.'</span>';
			$x++;
		}
		return $query;
	}
	public function gReservation(){
		$this->db->join('tempat','tempat.id_tempat=reservation.id_tempat');
		$query = $this->db->get('reservation');
		return $query;
	}


}


